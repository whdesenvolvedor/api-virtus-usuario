package br.com.azi.api.virtus.usuario.resources;

import br.com.azi.api.virtus.usuario.services.UsuarioService;
import br.com.azi.virtus.commons.dto.ResponseModel;
import br.com.azi.virtus.commons.entities.Usuario;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.net.URISyntaxException;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriBuilderException;
import javax.ws.rs.core.UriInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * REST Web Service
 *
 * @author rtayron
 */
@Path("usuarios")
@Api(value = "Usuario Resource", protocols = "http", tags = "Usuario")
public class UsuarioResource {

    private final Logger log = LogManager.getLogger(getClass());

    @EJB
    UsuarioService service;

    @ApiOperation(
            value = "Criar usuário",
            consumes = MediaType.APPLICATION_JSON,
            produces = MediaType.APPLICATION_JSON)
    @ApiResponses(
            @ApiResponse(
                    code = 201,
                    message = "Usuário criado.",
                    response = Usuario.class,
                    responseHeaders
                    = @ResponseHeader(
                            name = "Location",
                            description = "URI do novo usuário",
                            response = String.class)))
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response salvarUsuario(
            @ApiParam(
                    value = "Usuario",
                    name = "usuario",
                    required = true) Usuario usuario, @Context UriInfo uriInfo) throws URISyntaxException {
        try {
            // Se o usuário já existir, será desativado.
            Usuario usuarioPorCpf = service.buscarUsuarioPorCpf(usuario.getCpf());
            if (usuarioPorCpf != null) {
                service.salvarUsuario(usuarioPorCpf, Boolean.FALSE);
            }

            // Um novo usuário ATIVO é gerado.
            UriBuilder uri = uriInfo.getAbsolutePathBuilder();
            uri.path(service.salvarUsuario(usuario, Boolean.TRUE));
            return Response.created(uri.build()).build();
        } catch (IllegalArgumentException | UriBuilderException ex) {
            log.error(ex, ex);
            return Response.serverError().build();
        }
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Buscar todo os usuários",
            consumes = MediaType.APPLICATION_JSON,
            produces = MediaType.APPLICATION_JSON)
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "",
                    response = ResponseModel.class))
    public Response buscarUsuarios() {
        try {
            ResponseModel<Usuario> response = new ResponseModel<>();
            response.getDados().addAll(service.buscarUsuarios());
            return Response.ok().entity(response).build();
        } catch (Exception ex) {
            log.error(ex, ex);
            return Response.serverError().build();
        }
    }

    @ApiOperation(
            value = "Buscar usuário por cpf",
            consumes = MediaType.APPLICATION_JSON,
            produces = MediaType.APPLICATION_JSON)
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "",
                    response = ResponseModel.class))
    @GET
    @Path("{cpf}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarUsuarioPorCpf(
            @PathParam(value = "cpf") String cpf) throws URISyntaxException {
        try {
            ResponseModel<Usuario> model = new ResponseModel<>();
            model.getDados().add(service.buscarUsuarioPorCpf(cpf));
            return Response.ok().entity(model).build();
        } catch (Exception ex) {
            log.error(ex, ex);
            return Response.serverError().build();
        }
    }

}
