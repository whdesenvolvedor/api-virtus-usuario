package br.com.azi.api.virtus.usuario.resources;

import io.swagger.jaxrs.config.BeanConfig;
import java.util.Set;
import javax.ws.rs.core.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author wmendes
 */
@javax.ws.rs.ApplicationPath("v1")
public class ApplicationConfig extends Application {

    private final Logger log;

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        try {
            Class jsonProvider = Class.forName("org.glassfish.jersey.jackson.JacksonFeature");
            resources.add(jsonProvider);
        } catch (ClassNotFoundException ex) {
            log.trace("Erro ao adicionar o provider do Json", ex);
        }
        addRestResourceClasses(resources);
        addMyRestResourceClasses(resources);
        return resources;
    }

    public ApplicationConfig() {
        this.log = LogManager.getLogger(getClass());
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0");
        beanConfig.setTitle("API-VIRTUS-USUARIO");
        beanConfig.setDescription("Gestão de usuários virtus");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setBasePath("/api-virtus-usuario/v1");
        beanConfig.setResourcePackage("br.com.azi.api.virtus.usuario.resources");
        beanConfig.setScan(true);
    }

    private void addMyRestResourceClasses(Set<Class<?>> resources) {
        resources.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        resources.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);

    }

    /**
     * Do not modify addRestResourceClasses() method. It is automatically
     * populated with all resources defined in the project. If required, comment
     * out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(br.com.azi.api.virtus.usuario.resources.UsuarioResource.class);
    }

}
