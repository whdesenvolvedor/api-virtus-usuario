package br.com.azi.api.virtus.usuario.dao;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.mongodb.WriteResult;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.FindOptions;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.QueryResults;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;

/**
 *
 * @author wmendes
 * @param <T>
 */
@LocalBean
@Stateless
public class MongoDAO<T> {

    private Morphia morphia;
    private Datastore ds;
    private MongoClient client;

    /**
     * Usado para testes com BD local.
     */
    @PostConstruct
    public void build() {
        morphia = new Morphia();
        client = new MongoClient();
        morphia.mapPackage("br.com.azi.virtus.commons.entities");
        ds = morphia.createDatastore(client, "test");
        ds.ensureIndexes();
    }

//    @PostConstruct
//    public void build() {
//        morphia = new Morphia();
//        MongoClientOptions.Builder options_builder = new MongoClientOptions.Builder();
//        options_builder.maxConnectionIdleTime(256);
//        MongoClientOptions options = options_builder.build();
//        ServerAddress server = new ServerAddress("10.100.10.73",27020);
//        client = new MongoClient(server, options);
//        morphia.mapPackage("br.com.azi.virtus.commons.entities");
//        ds = morphia.createDatastore(client, "virtus");
//        ds.ensureIndexes();
//    }

    public Datastore getDs() {
        return ds;
    }

    public long count(Class clazz) {
        return ds.getCount(clazz);
    }

    public Query find(Class clazz) {
        return ds.createQuery(clazz);
    }

    public QueryResults find(Query query) {
        return query;
    }

    public WriteResult delete(T entity) {
        return ds.delete(entity);
    }

    public WriteResult deleteById(ObjectId id, Class clazz) {
        return ds.delete(clazz, id);
    }

    public WriteResult deleteByQuery(Query query) {
        return ds.delete(query);
    }

    public boolean exists(Query query) {
        return query.get(new FindOptions().limit(1)) != null;
    }

    public T get(ObjectId id, Class clazz) {
        return (T) ds.get(clazz, id);
    }

    public Key<T> save(T entity) {
        return ds.save(entity);
    }

    public UpdateResults update(T entity, final UpdateOperations<T> ops) {
        return ds.update(entity, ops);
    }

}
