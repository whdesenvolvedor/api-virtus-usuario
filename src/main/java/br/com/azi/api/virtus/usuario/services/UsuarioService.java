package br.com.azi.api.virtus.usuario.services;

import br.com.azi.api.virtus.usuario.dao.MongoDAO;
import br.com.azi.virtus.commons.entities.Usuario;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.QueryResults;

/**
 *
 * @author wmendes
 */
@LocalBean
@Stateless
public class UsuarioService {

    @EJB
    MongoDAO<Usuario> dao;

    public String salvarUsuario(Usuario usuario, Boolean ativo) {
        if (Boolean.TRUE.equals(ativo)) {
            usuario.setHash(stringHexa(gerarHash(usuario)));
        }

        usuario.setAtivo(ativo);
        Key<Usuario> obj = dao.save(usuario);
        Usuario c = dao.get((ObjectId) obj.getId(), Usuario.class);
        return c.getCpf();
    }

    public List<Usuario> buscarUsuarios() {
        Query<Usuario> query = dao.getDs().createQuery(Usuario.class);
        query.and(query.criteria("ativo").equal(true));
        QueryResults find = dao.find(query);
        return (List<Usuario>) find.get();
    }

    public Usuario buscarUsuarioPorCpf(String cpf) {
        Query<Usuario> query = dao.getDs().createQuery(Usuario.class);
        query.and(query.criteria("cpf").equal(cpf));
        query.and(query.criteria("ativo").equal(true));
        QueryResults find = dao.find(query);
        return (Usuario) find.get();
    }

    private String stringHexa(byte[] bytes) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            int parteAlta = ((bytes[i] >> 4) & 0xf) << 4;
            int parteBaixa = bytes[i] & 0xf;
            if (parteAlta == 0) {
                s.append('0');
            }
            s.append(Integer.toHexString(parteAlta | parteBaixa));
        }
        return s.toString();
    }

    private byte[] gerarHash(Usuario usuario) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(usuario.toString().getBytes());
            return md.digest();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

}
